<?php

use Page\Acceptance\LoginPage;
use Step\Acceptance\LoginStep;

class LoginCest
{
    public function show_home_page_when_data_login_successfully(LoginStep $loginStep, LoginPage $loginPage)
    {
        $loginStep->login('sang.hv@deha-soft.com', '12345678');
        $loginPage->checkRememberMe()->clickLoginButton();
        $loginStep->retrySee('Dashboard');
    }

    public function show_error_message_when_data_login_blank(LoginStep $loginStep, LoginPage $loginPage)
    {
        $loginStep->login('', '');
        $loginPage->clickLoginButton();
        $loginStep->retrySee('Login');
    }

    public function show_error_message_when_data_email_blank(LoginStep $loginStep, LoginPage $loginPage)
    {
        $loginStep->login('', '12345678');
        $loginPage->clickLoginButton();
        $loginStep->retrySee('Login');
    }

    public function show_error_message_when_data_email_error(LoginStep $loginStep, LoginPage $loginPage)
    {
        $loginStep->login('sang.hvdeha-soft.com', '12345678');
        $loginPage->clickLoginButton();
        $loginStep->retrySee('Login');
    }

    public function show_error_message_when_data_password_blank(LoginStep $loginStep, LoginPage $loginPage)
    {
        $loginStep->login('sang.hv@deha-soft.com', '');
        $loginPage->clickLoginButton();
        $loginStep->retrySee('Login');
    }

    public function show_error_message_when_data_password_error(LoginStep $loginStep, LoginPage $loginPage)
    {
        $loginStep->login('sang.hv@deha-soft.com', '00000000');
        $loginPage->clickLoginButton();
        $loginStep->retrySee('These credentials do not match our records.');
    }
}
