<?php

use Page\Acceptance\RegisterPage;
use Step\Acceptance\RegisterStep;

class RegisterCest
{
    public function register_successfully(RegisterStep $registerStep, RegisterPage $registerPage)
    {
        $registerStep->register('Nhung', 'thanhhoa711@gmail.com', '+84839484950', 'hongnhung114', 'hongnhung114');
        $registerPage->clickRegisterButton();
        $registerStep->retrySee('Dashboard');
    }
    public function register_name_blank(RegisterStep $registerStep, RegisterPage $registerPage)
    {
        $registerStep->register('', 'maihoang10296@gmail.com', '+84839484951', 'hongnhung112', 'hongnhung112');
        $registerPage->clickRegisterButton();
        $registerStep->retrySee('Register');
    }
    public function register_email_blank(RegisterStep $registerStep, RegisterPage $registerPage)
    {
        $registerStep->register('Ly', '', '+84839484952', 'hongnhung113', 'hongnhung113');
        $registerPage->clickRegisterButton();
        $registerStep->retrySee('Register');
    }
    public function register_phone_blank(RegisterStep $registerStep, RegisterPage $registerPage)
    {
        $registerStep->register('Nhung1', 'thuonghuong2609@gmail.com', '', 'hongnhung115', 'hongnhung115');
        $registerPage->clickRegisterButton();
        $registerStep->retrySee('Register');
    }
    public function register_password_blank(RegisterStep $registerStep, RegisterPage $registerPage)
    {
        $registerStep->register('Nhung1', 'thuonghuong2709@gmail.com', '+84335427567', '', 'hongnhung115');
        $registerPage->clickRegisterButton();
        $registerStep->retrySee('Register');
    }
    public function register_passwordconfirm_blank(RegisterStep $registerStep, RegisterPage $registerPage)
    {
        $registerStep->register('Nhung2', 'thuonghuong2719@gmail.com', '+84335427567', 'hongnhung116', '');
        $registerPage->clickRegisterButton();
        $registerStep->retrySee('Register');
    }

    public function register_email_error(RegisterStep $registerStep, RegisterPage $registerPage)
    {
        $registerStep->register('Nhung3', 'thuonghuong2719@gmail#$%&*^', '+84335427566', 'hongnhung116', 'hongnhung116');
        $registerPage->clickRegisterButton();
        $registerStep->retrySee('Register');
    }

    public function register_email_and_password_error(RegisterStep $registerStep, RegisterPage $registerPage)
    {
        $registerStep->register('Nhungly', 'thuonghuong2701gmail.com', '+84987805741', '!@#$%^&*(', '!@#$%^&*(');
        $registerPage->clickRegisterButton();
        $registerStep->retrySee('Register');
    }

    public function register_phone_error(RegisterStep $registerStep, RegisterPage $registerPage)
    {
        $registerStep->register('Nhungly', 'thuonghung123@gmail.com', '000789124n', 'hongnhung1234', 'hongnhung1234');
        $registerPage->clickRegisterButton();
        $registerStep->retrySee('Dashboard');
    }

    public function register_password_error(RegisterStep $registerStep, RegisterPage $registerPage)
    {
        $registerStep->register('Nhungly1', 'thuonghung2345@gmail.com', '+84335427889', 'aaaaa', 'aaaaa');
        $registerPage->clickRegisterButton();
        $registerStep->retrySee('The password must be at least 8 characters');
    }

    public function register_passwordconfirm_error(RegisterStep $registerStep, RegisterPage $registerPage)
    {
        $registerStep->register('Nhungly2', 'thuonghung2645@gmail.com', '+84335427888', 'hongnhung11', 'hongnhung115');
        $registerPage->clickRegisterButton();
        $registerStep->retrySee('The password confirmation does not match');
    }
}
