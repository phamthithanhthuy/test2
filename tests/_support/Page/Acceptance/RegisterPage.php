<?php
namespace Page\Acceptance;

class RegisterPage
{
    // include url of current page
    public static $URL = '/register';

    const NAME_FIELD = '//input[@id="name"]';
    const EMAIL_ADDRESS_FIELD = '//input[@id="email"]';
    const PHONE_FIELD = '//input[@id="phone"]';
    const PASSWORD_FIELD = '//input[@id="password"]';
    const CONFIRM_PASSWORD_FIELD = '//input[@id="password-confirm"]';
    const BUTTON_REGISTER = '//button[contains(text(),"Register")]';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    /**
     * @var \AcceptanceTester;
     */
    protected $acceptanceTester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->acceptanceTester = $I;
    }

    public function onPage(){
        $this->acceptanceTester->amOnPage(self::$URL);
        return $this;
    }

    public function fillName($name){
        $this->acceptanceTester->retryFillField(self::NAME_FIELD, $name);
        return $this;
    }

    public function fillEmail($email){
        $this->acceptanceTester->retryFillField(self::EMAIL_ADDRESS_FIELD, $email);
        return $this;
    }

    public function fillPhone($phone){
        $this->acceptanceTester->retryFillField(self::PHONE_FIELD, $phone);
        return $this;
    }

    public function fillPassword($password){
        $this->acceptanceTester->retryFillField(self::PASSWORD_FIELD, $password);
        return $this;
    }

    public function fillPasswordconfirm($passwordconfirm){
        $this->acceptanceTester->retryFillField(self::CONFIRM_PASSWORD_FIELD, $passwordconfirm);
        return $this;
    }

    public function clickRegisterButton(){
        $this->acceptanceTester->retryClick(self::BUTTON_REGISTER);
        return $this;
    }

}