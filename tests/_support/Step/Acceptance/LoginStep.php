<?php
namespace Step\Acceptance;

use Page\Acceptance\LoginPage;

class LoginStep extends \AcceptanceTester
{
    public function login($email, $password){
        $loginPage = new LoginPage($this);
        $loginPage->onPage()
            ->fillEmail($email)
            ->fillPassword($password);
    }
}